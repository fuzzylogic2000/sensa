#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import numpy as np
import scipy as sy
import scipy.integrate
import sympy as sm
import model_import as mi
import matplotlib.pyplot as plt
import re

class mca_model:
	"""class for loading an sbml model and applying metabolic
	control analysis.\n Functionality:\n\tSimple stuff: Return
	species, reactions, parameters, initial conditions,
	stoichiometrix matrix, species equation system\n\tAdvanced
	stuff: Generalized (implicite) Substrate and parameter
	elasticities, finding a steady state from initial conditions
	and returning st st elasticities and fluxes, building the ODE
	system for time dependend response coefficients and
	calculating them in general and in the steady state. Plot
	species and response coefficients\n\n\tArguments: path (path
	to the .xml file)\n\n\tFor further comments on the code, have
	a look into resp_coeff.py"""
	def __init__(self, path):
		self.path=path
		self.model=self.load_model()
		self.species=self.model.species
		self.r=len(self.model.reactions)
		self.s=len(self.model.species)
		self.reactions=self.model.reactions
		self.reaction_list=self.model.reaction_list
		self.inis=self.model.species_values
		self.par=self.model.parameters
		self.p=len(self.par)
		self.pardict=self.model.parameter_values
		self.parspec=self.par+self.species
		self.q=self.s+self.p
		self.N=sm.Matrix(self.model.N)
		self.v=self.v()
		self.v_exp=self.v.subs(self.pardict)
		self.dsdt=self.N*self.v
		self.dsdt_0=sm.zeros((self.s,1))
		for i,sp in enumerate(self.species):
			self.dsdt_0[i]=self.inis[sp]
		self.dsdq_0=sm.zeros((self.s,self.q))
		for i in range(self.s):
			for j in range(self.q):
				if i==j-self.p: self.dsdq_0[i,j]=1
		self.specODE = self.build_specODE_func()
		self.substrate_elasticities=self.dvds=self.substrate_elasticities()
		self.parameter_elasticities=self.dvdq=self.parameter_elasticities()
		self.resp_coeff_template=self.dsdq_template=self.resp_coeff_template()
		self.RCs_and_spec=self.species+list(self.resp_coeff_template)
		self.d_dsdq_dt=self.d_dsdq_dt()
		d_dsdq_dt_list=list(self.d_dsdq_dt)
		dsdt_list=list(self.dsdt.subs(self.pardict))
		self.ODE_syst= dsdt_list+d_dsdq_dt_list
		self.bigODE = self.build_bigODE()
		
	def load_model(self):
		"""Is a class attribute (executed in main method)\n"""
		return mi.my_model(self.path)
	
	def v(self):
		"""Is a class attribute (executed in main
		method).\nreturns the vector of implicit reaction
		rates"""
		v=sm.zeros((self.r,1))
		for i,sp in enumerate(self.reaction_list):
			v[i]=self.reactions[sp]
		return v
	
	def simulate_species(self,time,stepsize,initial=None):
		"""Takes the simulation time and the step size and
		simulates the time course of the species for the given
		initial conditions\n\nArguments:\n\ttime: integer
		corresponding to the simulation time\n\tstepsize:
		number corresponding to the interval size between two
		time steps)\n\tinitial: list or array of initial
		conditions. Default: initial conditions of the model
		"""
		exec self.specODE
		if initial == None:
			print "No initial conditions given. Will take model's initial conditions by default"
			S0=list(self.dsdt_0)
		else: S0 = list(initial)
		t=sy.linspace(0,time,(1/stepsize)*time)
		result=sy.integrate.odeint(dS_dt, S0, t)
		self.species_simulation=result
		return result
	
	def find_steady_state(self,time=15,tol=1e-6, initial_state=None):
		"""Takes an optional initial state, a time argument,
		that characterizes the interval between two function
		evaluations in the optimization and a tolerance. The
		tolerance indicates when the steady state is reached.
		In each step, it is compared to the norm of the
		difference of 2 consecutive time step vectors divided
		by the vector length (number of
		species)\n\nArguments:\n\ttime: integer corresponding
		to the step size of integration for the steady state
		search, Default=15\n\ttol: relative tolerance of one
		state to the previous one defining when the steady
		state will be found, Default=1e-6\n\tinitial_state:
		list or array of initial conditions to start the
		search from. Default=initial conditions of the model"""
		exec self.specODE
		if initial_state==None: 
			print "No initial conditions given. Will take model's initial conditions by default"
			init=list(self.dsdt_0)
		else: init=inital_state
		result=[init]
		evaltime=0
		while 1:
			t=sy.linspace(0,time,2)
			S0=result[-1]
			result = sy.integrate.odeint( dS_dt, S0, t )
			print "Steady state deviation: " + str(np.linalg.norm( result[0] - result[1] ) / len(S0))
			if (np.linalg.norm( result[0] - result[1] ) / len(S0)) < tol:
				break
			if evaltime>500000:
				raise Exception('No steady state could be found')
			evaltime+=time
		self.st_st=result[1]
		return result[1]
		
	def substrate_elasticities(self):
		"""Is a class attribute (executed in main
		method).\nreturns the explicit (parameters substituted
		with their values) substrate elasticities (dv/ds)"""
		dvds=sm.zeros((self.r,self.s))
		for i,re in enumerate(self.reaction_list):
			for j,sp in enumerate(self.species):
				dvds[i,j]=sm.diff(self.reactions[re],sp)
		return dvds.subs(self.pardict)
	
	def parameter_elasticities(self):
		"""Is a class attribute (executed in main
		method).\nreturns the explicit (parameters substituted
		with their values) parameter elasticities (dv/dq)"""
		dvdq=sm.zeros((self.r,self.q))
		for i,re in enumerate(self.reaction_list):
			for j,sp in enumerate(self.par):
				dvdq[i,j]=sm.diff(self.reactions[re],sp)
		return dvdq.subs(self.pardict)
	
	def st_st_flux(self):
		"""If a steady state has been found previously with
		the function find_stead_state(), it returns the steady
		state flux, i.e. the value of the reaction velocity
		vector substituted by the steady state concentrations"""
		try: self.st_st
		except: raise Exception('Class has no steady state yet. Run method find_steady_state() first')
		ssdict={}
		for i,s in enumerate(self.species):
			ssdict[s]=self.st_st[i]
		return self.v_exp.subs(ssdict)
	
	def st_st_substrate_elasticitites(self):
		"""If a steady state has been found previously with
		the function find_stead_state(), it returns the steady
		state substrate elasticities, (i.e. substituted steady
		state concentrations)"""
		try: self.st_st
		except: raise Exception('Class has no steady state yet. Run method find_steady_state() first')
		ssdict={}
		for i,s in enumerate(self.species):
			ssdict[s]=self.st_st[i]
		return self.substrate_elasticities.subs(ssdict)
		
	def st_st_parameter_elasticitites(self):
		"""If a steady state has been found previously with
		the function find_stead_state(), it returns the steady
		state parameter elasticities, (i.e. substituted steady
		state concentrations)"""
		try: self.st_st
		except: raise Exception('Class has no steady state yet. Run method find_steady_state() first')
		ssdict={}
		for i,s in enumerate(self.species):
			ssdict[s]=self.st_st[i]
		return self.parameter_elasticities.subs(ssdict)
		
	def build_specODE_func(self):
		"""Is a class attribute (executed in main
		method).\nBuilds up the string for the species' ODE
		system, which can be executed later to yield the
		function dS/dt that can be integrated in the functions
		simulate_species(), find_steady_state()"""
		dsdt_str=str(list(self.dsdt.subs(self.pardict)))
		for i,s in enumerate(self.species):
			#S = re.compile(r'%s(?=[^0-9_]|$)'%(s))
			S = re.compile(r'%s(?=\W|$)'%(s))
			dsdt_str = S.sub('S[%s]'%(i),dsdt_str)
		dsdt_str=dsdt_str.replace(",",",\n\t")
		
		str0="from numpy import array\ndef dS_dt(S,t):\n\t return np.array("+dsdt_str+")"
		return str0
	
	def resp_coeff_template(self):
		"""Is a class attribute (executed in main method).\nBuilds a template for the big (s x q) response coefficients matrix"""
		dsdq=sm.zeros((self.s,self.q))
		for i,sp in enumerate(self.species):
			for j,param in enumerate(self.parspec):
				#if (j<self.p): dsdq[i,j]= "RC_" + sp + "_"+ param
				#if (j>=self.p): dsdq[i,j]= "RC_" + sp + "_" + param + "_0"
				dsdq[i,j]= "RC_" + str(i) + "_"+ str(j)
		return dsdq
		
	def d_dsdq_dt(self):
		"""Is a class attribute (executed in main method).\nCalculates the ODE for the response coefficients following the formula: d(dsdq)/dt=N*dv/ds*ds/dq + dv/dq (Ingalls et al. 2002)"""
		d_dsdq_dt=self.N*(self.dvds*self.dsdq_template+self.dvdq)
		return d_dsdq_dt
	
	def build_bigODE(self):
		"""Is a class attribute (executed in main
		method).\nBuilds up the string for the response
		coefficients' ODE system, which can be executed later
		to yield the function dX/dt that can be integrated in
		the functions time_dep_resp_coeff(),
		time_dep_resp_coeff_selectedpar()"""
		str1=str(self.ODE_syst)
		for i,s in enumerate(self.RCs_and_spec):
			resp = re.compile(r'%s(?=\W|$)'%(s))
			str1 = resp.sub('resp[%s]'%(i),str1)
		str1=str1.replace(",",",\n\t")
		str0="from numpy import array\ndef dX_dt(resp,t):\n\t return np.array("+str1+")"
		return str0
		
	def time_dep_resp_coeff(self,time,stepsize,species_inis=None):
		"""Calculates ALL the time dependent response
		coefficients (attention! long computational time!)
		over a certain time with a certain step
		size\n\nArguments:\n\ttime: integer corresponding to
		the integration time\n\tstepsize: number corresponding
		to the interval size between two consecutive time
		steps\n\tspecies_inis: Initial conditions for the
		species. Default: Initial conditions of the model"""
		exec self.bigODE
		if species_inis==None: 
			print "No initial conditions for species given, will take model's initial conditions"
			X0=list(self.dsdt_0)	+ list(self.dsdq_0)
		else: X0=list(species_inis)	+ list(self.dsdq_0)
		t=sy.linspace(0,time,(1/stepsize)*time)
		result=sy.integrate.odeint(dX_dt, X0, t)
		self.time_dep_resp_coeff=result
		return result
	
	"""	
	def ODE_syst_selectedpar(self,parname):
		ODE_syst_new=self.ODE_syst[:self.s]
		a=re.compile(r'%s(?=\W|$)'%(parname))
		for i,od in enumerate(str(self.ODE_syst).split(',')):
			if len(a.findall(od))!=0 :
				ODE_syst_new.append(self.ODE_syst[i])
		return ODE_syst_new
	"""	
	
	def time_dep_resp_coeff_selectedpar(self, parname, time,stepsize):
		"""Calculates the time dependent response coefficients
		for only one selected parameter: computationally much
		less exhaustive than for all the response
		coefficients.\n\nArguments:\n\tparname: Name (as
		string) of the parameter in question\n\ttime: integer
		corresponding to the integration time\n\tstepsize:
		number corresponding to the interval size between two
		consecutive time steps\n\t Code can probably be
		optimized"""
		ODE_syst_new=self.ODE_syst[:self.s]
		a=re.compile(r'%s(?=\W|$)'%(parname))
		for i,od in enumerate(str(self.ODE_syst).split(',')):
			if len(a.findall(od))!=0 :
				ODE_syst_new.append(self.ODE_syst[i])
		for i,s in enumerate(self.dsdt):
			if s==0: ODE_syst_new.insert(self.s+i,0)
		RCs_and_spec_reduced=(self.RCs_and_spec[:self.s])
		a=re.compile(r'%s(?=\W|$)'%(parname))
		for i,rc in enumerate(str(self.RCs_and_spec).split(",")):
			if len(a.findall(rc))!=0: RCs_and_spec_reduced.append(self.RCs_and_spec[i])
		str1=str(ODE_syst_new)
		for i,s in enumerate(RCs_and_spec_reduced):
			resp = re.compile(r'%s(?=\W|$)'%(s))
			str1 = resp.sub('resp[%s]'%(i),str1)
		str1=str1.replace(",",",\n\t")
		str0="def dX_dt(resp,t):\n\t return np.array("+str1+")"
		exec str0
		pos=self.par.index(parname)
		X0=list(self.dsdt_0)
		X0.extend(list(self.dsdq_0[:,pos]))
		t=sy.linspace(0,time,(1/stepsize)*time)
		result=sy.integrate.odeint(dX_dt, X0, t)
		varname="self.rc_"+parname
		vars()[varname]=result
		return result
		
	def st_st_resp_coeff(self):
		""""""
		try: self.st_st
		except: raise Exception('Class has no steady state yet. Run method find_steady_state() first')
		try: self.time_dep_resp_coeff
		except: raise Exception('Class has no implicitly calculated response coefficients yet. Run method time_dep_resp_coeff() first')
		
		
	
	def plot_species(self,timerange):
		"""Plots the species' time course for a certain time
		range. If the class has no solution for the species
		ODE yet (either from simulate_species() or from
		time_dep_resp_coeff(), it gives an error message and
		tells you to run it first.\n\nArguments:\n\ttimerange:
		The time range as a list for which to plot the
		simulated species"""
		try: 
			self.species_simulation
			plt.plot(self.species_simulation)
			plt.show()
		except: raise Exception('Class has no solution for species ODE yet, run method simulate_species() first')
		try:
			self.time_dep_resp_coeff
			plt.plot(self.time_dep_resp_coeff[timerange,:self.s])
			plt.show()
		except: raise Exception('Class has no solution for resp_coeff ODE yet, run method simulate_species() or time_dep_resp_coeff() first')
		
		
	def plot_resp_coeff(self,timerange):
		"""Plots the response coefficients' time course as
		long as there is a solution for them yet (from
		time_dep_resp_coeff())\n\nArguments:\n\ttimerange: The
		time range as a list for which to plot the simulated
		species"""
		if type(timerange)==int:
			timerange=range(timerange)
		try: self.time_dep_resp_coeff
		except: raise Exception('Class has no implicitly calculated response coefficients yet. Run method time_dep_resp_coeff() first')
		plt.plot(self.time_dep_resp_coeff[timerange,self.s:])
		plt.show()
		
