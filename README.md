SensA
==========

Sensa provides metabolic control analysis for
[SBML](http://www.sbml.org) models of biological systems in one
convenient package. Sensa is built in Python and can be run locally or
through our [web-interface](http://gofid.biologie.hu-berlin.de) which also
provides handy visualisations of the outcomes.

The core package has the following features:

* Import and simulation of SBML models
* Find elasticities
* Get control coefficients
* Calculate response coefficients
* Find and plot time dependent response coefficients

