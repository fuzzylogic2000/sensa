Template.tabs.tabs = function() {
    var ana_list = Analysis.findOne({model_id: Session.get("selected_model")},
                                    {fields: {model_id:0,
                                              owner: 0}});
    var has_data = false,
        has_error = false,
        has_warning = false,
        result = [];

    for (var key in ana_list) {
        if (ana_list[key].hasOwnProperty('data'))
            has_data = true;
        else
            has_data = false;
        try {
            if (ana_list[key].error.length === 0)
                has_error = false;
            else
                has_error = true;
        }
        catch(e){
            has_error = false;
        }
        try {
            if (ana_list[key].warning.length === 0)
                has_warning = false;
            else
                has_warning = true;
        }
        catch(e){
            has_warning = false;
        }

        result.push({name: ana_list[key]['name'],
                     priority: ana_list[key]['priority'],
                     value: key,
                     contains_errors: has_error,
                     contains_warnings: has_warning,
                     computed: has_data});
    }

    return _.sortBy(result, 'priority');
};

Template.tabs.is_computed = function() {
    return this.computed ? "active_link" : "";
};

Template.tabs.has_errors = function() {
    return this.contains_errors ? "text-error" : "";
};

Template.tabs.has_warnings = function() {
    return this.contains_warnings ? "text-warning" : "";
};

Template.tabs.events({
    'click .active_link': function(event) {
        Session.set("selected_analysis", this.value);
    }
});

Template.tabs.selected = function() {
    return Session.equals("selected_analysis", this.value) ? "active" : '';
};